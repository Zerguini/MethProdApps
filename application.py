#! /usr/bin/env python3

from flask import Flask, render_template
from db import DB
app = Flask(__name__)
app.debug = True

Gtodo={
	'None':["rien","grand chose","sieste"],
	'bob':["Python","manger","git"],
	'alice':["fumer","teaser","baiser"],
	'bebert':["peche","ricard","tpaze"]}
@app.route('/')
def home():
    return render_template(
        "home.html")

@app.route('/user/')
@app.route('/user/<name>')
def user(name=None):
    db=DB()
    todo=db.get(name)
    return render_template(
        "user.html",
        name=name,
        todo=todo)

@app.route('/users/')
def users():
    db=DB()
    users=db.users()
    return render_template(
        "users.html",data=users)

if __name__ == '__main__':
    app.run(host="0.0.0.0")
